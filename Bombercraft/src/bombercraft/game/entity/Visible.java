package bombercraft.game.entity;

import utils.GVector2f;

public interface Visible {
	public GVector2f getPosition();
	public GVector2f getSize();
}
